#!/bin/sh
VMATH_VERSION=0.13

DIR=$(cd $(dirname $0); pwd)
OLD_DIR=$(pwd)

cd $DIR

# build and run tests
mkdir -p build && cd build && cmake .. && make && cd ..
build/vmath_tests > tests/testAll.log
if [ $(cat tests/testAll.log | wc -l) -gt 7 ]; then
    echo "ERROR: Unit tests seem to failed!"
    cat tests/testAll.log
    cd $OLD_DIR
    exit 1
fi

rm -rf build

# Generate docs
doxygen
rm doc/html/*.map doc/html/*.md5
patch doc/latex/refman.tex < refman.tex.patch
make -C doc/latex


mkdir -p tmp/vmath-${VMATH_VERSION}/doc
cp Doxyfile LICENSE CMakeLists.txt Makefile refman.tex.patch tmp/vmath-${VMATH_VERSION}
cp -r src/ tmp/vmath-${VMATH_VERSION}/src
cp -r tests/ tmp/vmath-${VMATH_VERSION}/tests
cp -r doc/html tmp/vmath-${VMATH_VERSION}/doc/html
cp doc/latex/refman.pdf tmp/vmath-${VMATH_VERSION}/doc/vmath-${VMATH_VERSION}_api_reference_manual.pdf
cd tmp/ && tar cvfj vmath-${VMATH_VERSION}.tar.bz2 vmath-${VMATH_VERSION}/
rm -rf tmp/vmath-${VMATH_VERSION}

cd $OLD_DIR
